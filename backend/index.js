const mongoose = require('mongoose')
const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const AuthRouter = require('./routes/authRouter')
const UserRouter = require('./routes/userRouter')
const FilterRouter = require('./routes/filterRouter')
const cloudinary = require('cloudinary');
var serveStatic = require('serve-static');
const busboyBodyParser = require('busboy-body-parser');
const path = require('path')

require('./modules/telegram')

app.use(serveStatic(path.join(__dirname, '../dist')));

cloudinary.config({
    cloud_name: 'teth',
    api_key: '888189433738919',
    api_secret: 'pPYXkzSevNKjmA2JBG5XHT2vamc'
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(busboyBodyParser({ limit: '15mb' }));

app.use('/api/auth', AuthRouter);
app.use('/api/users', UserRouter);
app.use('/api/filters', FilterRouter);

const databaseUrl = 'mongodb+srv://tethico:110900da@wftackdb.5mlmu.mongodb.net/wftackdb?retryWrites=true&w=majority'
const connectOptions = { useNewUrlParser: true };
const PORT = 1109

mongoose.connect(databaseUrl, connectOptions)
    .then(() => console.log(`DB connected: ${databaseUrl}`))
    .then(() => app.listen((process.env.PORT || PORT), () => { console.log(`listening on port ${PORT}`); }))
    .catch(err => console.log('Error on start' + err));
