const express = require('express');
const router = express.Router();
const auth = require('../auth')
const jwt = require('jsonwebtoken');
const Users = require('../modules/users')

router.post('/login', function (req, res) {
    // console.log(req.body)
    if (req.body.username && req.body.password) {
        Users.getById(req.body.username)
            .then(user => {
                if (user) {
                    // check pass NEED TO SEND HASH
                    if (user.passhash === auth.encr(req.body.password)) {
                        // give token
                        let token = jwt.sign(user.toJSON(), auth.sc, {
                            expiresIn: '24h'
                        })
                        res.send(token)
                        console.log('responded with token')
                    }
                } else {
                    // user not found need to register
                    res.status(401).send('User not found')
                }
            })
            .catch(err => {
                console.log('oopsie')
                console.log(err)
                res.status(500).send(err)
            })
    } else {
        res.send(400).send('No empty fields')
    }
});

router.post('/register', function (req, res) {
    res.redirect(307, '/api/users/new')
})

module.exports = router
