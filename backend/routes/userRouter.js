const express = require('express');
const router = express.Router();
const auth = require('../auth')
const Users = require('../modules/users')
const cloudinary = require('cloudinary');

cloudinary.config({
    cloud_name: 'teth',
    api_key: '888189433738919',
    api_secret: 'pPYXkzSevNKjmA2JBG5XHT2vamc'
});

const USERSPERPAGE = 5

router.get('/', function (req, res) {
    Users.getAll()
        .then((data) => {
            // console.log(data)
            let newPage = 1;
            if (req.query.page) {
                newPage = req.query.page;
            }
            let toShow = searchUsers(data, String(req.query.search));
            let totPages = Math.ceil(toShow.length / USERSPERPAGE)
            // console.log(toShow)
            toShow = splitArray(toShow, USERSPERPAGE, newPage);
            // console.log(toShow)
            return {
                users: toShow,
                totalPages: totPages
            };
        })
        .then(vusers => {
            res.json(vusers)
        })
        .catch(err => console.log(err));
});

router.get('/:id', function (req, res) {
    // console.log('got req for 1')
    Users.getById(req.params.id)
        .then((data) => {
            if (data === null) {
                // console.log('nope')
                res.status(404).send('No such user here')
            } else {
                // console.log('good')
                res.status(200).send(data)
            }
        })
});

router.delete('/:id/delete', function (req, res) {
    Users.remove(req.params.id)
        .then((data) => res.json(data))
        .catch(err => res.status(404).json(err))
});

router.patch('/:id/update', function (req, res) {
    // console.log(req.body)
    // let pic = req.files.image
    Users.getById(req.params.id)
        .then(user => {
            console.log('i upd')
            let newUser = user
            // console.log(user)
            if (req.body && req.body.telegramId) {
                newUser.telegramId = req.body.telegramId
            }
            if (req.body && req.body.chatId) {
                newUser.chatId = req.body.newChatId
            }
            // if (req.body && req.body.filters) {
                console.log(req.body.filters)
                newUser.filters = JSON.parse(req.body.filters)
                // console.log(newUser.filters)
            // }
            if (req.files && req.files.image) {
                let nowLong = new Date().getTime();
                const fileBuffer = req.files.image.data;
                cloudinary.v2.uploader.upload_stream({ public_id: nowLong },
                    function (error, result) {
                        if (error) {
                            console.log(error)
                        }
                        newUser.avaUrl = result.url
                        Users.update(user.login, newUser)
                            .then(() => {
                                console.log('1')
                                res.status(201).json(newUser)
                            })
                            .catch(err => {
                                console.log(err);
                                console.log('2')
                                res.status(409).json(err)
                            });
                    })
                    .end(fileBuffer);
            } else {
                Users.update(user.login, newUser)
                    .then(() => {
                        // console.log('1 nopic')
                        res.status(201).json(newUser)
                    })
                    .catch(err => {
                        console.log(err);
                        // console.log('2 nopic')
                        res.status(409).json(err)
                    });
            }
        })
})

router.post('/new', function (req, res) {
    // console.log(req.body);
    // console.log(req.files);
    let username = req.body.username;
    Users.getById(username)
        .then(user => {
            if (user) {
                console.log('we have one')
                res.status(409).json('There is already user with that username');
            } else {
                console.log(req.body)
                let password = req.body.password;
                let confirmPassword = req.body.confirmPassword;
                console.log(password)
                console.log(confirmPassword)
                if (password !== confirmPassword) {
                    console.log('pass err')
                    res.status(409).json();
                } else {
                    let nowLong = new Date().getTime();
                    if (req.files && req.files.image) {
                        const fileBuffer = req.files.image.data;
                        cloudinary.v2.uploader.upload_stream({ public_id: nowLong },
                            function (error, result) {
                                if (error) {
                                    console.log(error)
                                }
                                let newUsr = new Users.User(req.body.username, auth.encr(password), 0, new Date(), result.url);
                                Users.insert(newUsr)
                                    .then(() => {
                                        console.log('1')
                                        res.status(201).json(newUsr)
                                    })
                                    .catch(err => {
                                        console.log(err);
                                        console.log('2')
                                        res.status(409).json(err)
                                    });
                            })
                            .end(fileBuffer);
                    } else {
                        let newUsr = new Users.User(req.body.username, auth.encr(password), 0, new Date());
                        Users.insert(newUsr)
                            .then(() => {
                                res.status(201).json(newUsr)
                            })
                            .catch(err => {
                                console.log(err);
                                res.status(409).json(err)
                            });
                    }
                }
            }
        });
});

function splitArray(array, itemsPerPage, pageNum) {
    let start = (pageNum - 1) * itemsPerPage;
    let end = pageNum * itemsPerPage;
    if (end > array.length) {
        end = array.length
    }
    return array.slice(start, end);
}

function searchUsers(users, searchString) {
    let filteredUsers = [];
    if (!searchString || searchString === 'undefined') {
        filteredUsers = users
    } else {
        users.forEach(x => {
            let re = new RegExp(searchString, 'i');
            if (x.login.search(re) !== -1) {
                filteredUsers.push(x);
            }
        });
    }
    return filteredUsers;
}

module.exports = router
