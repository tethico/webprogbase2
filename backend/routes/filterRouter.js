const express = require('express');
const router = express.Router();
const Filters = require('../modules/filters')

router.get('/:id', function (req, res) {
    Filters.getById(req.params.id)
        .then(data => {
            res.status(200).json(data)
        })
})

router.post('/new', function (req, res) {
    let filter = new Filters.Filter('New filter', [], true)
    console.log(req.body)
    if (req.body.newFiterName) {
        filter.name = req.body.newFiterName
    }
    if (req.body.newFilters) {
        filter.filters = req.body.newFilters
    }
    if (req.body.newEnabled) {
        filter.enabled = req.body.newEnabled
    }
    console.log('flter')
    Filters.insert(filter)
        .then(data => {
            res.status(200).json(data)
        })
})

router.patch('/:id/update', function (req, res) {
    // console.log(req.body.newFilters)
    let newf = new Filters.Filter(req.body.newFilterName, JSON.parse(req.body.newFilters), req.body.newEnabled)
    // console.log(req.body)
    Filters.getById(req.params.id)
        .then(data => {
            // console.log(newf)
                Filters.update(data._id, newf)
                    .then(data => {
                        // console.log(data)
                        res.status(200).json(newf)
                    })
            }
        )
        .catch(() => {
            res.status(307).redirect('/api/filters/new')
        })
})
router.delete('/:id/delete', function(req, res) {
    Filters.remove(req.params.id)
        .then(data => {
            console.log(data)
            res.status(200).json(data)
        })
        .catch(() => {
            res.status(404).json('No filter found to delete')
        })
})

module.exports = router
