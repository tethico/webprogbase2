const axios = require('axios')
// const config = require('../config')
const Users = require('./users')
const Filters = require('./filters')

const TelegramBotApi = require('telegram-bot-api');

const telegramBotApi = new TelegramBotApi({
    token: '715531033:AAHwcSZdj1j0I0O0Xbz5MXPeHNYH8_N4fY4',
    updates: {
        enabled: true // do message pull
    }
});

let chatIdsToSendAlerts = []

let currentEvents = []
let currentSentEvents = []

setInterval(GetEvents, 2 * 60 * 1000);

telegramBotApi.on('message', onMess)

function onMess(message) {
    processRequest(message)
        .catch(() => telegramBotApi.sendMessage({
            chat_id: message.chat.id,
            text: 'Something went wrong. Try again later.'
        }))
}

const AL_CAPT = ' To get more information about this alert visit WFTrack'

async function GetEvents() {
    await axios.get('https://api.warframestat.us/pc/alerts')
        .then(response => {
            currentEvents = []
            response.data.forEach(alert => {
                currentEvents.push(alert.id)
                if (currentSentEvents.includes(alert.id)) {
                } else {
                    currentSentEvents.push(alert.id)
                    chatIdsToSendAlerts.forEach(chatid => {
                        Users.findByChatId(chatid)
                            .then(user => {
                                if (user.filters.length) {
                                    // console.log('filter')
                                    user.filters.forEach(flt => {
                                        Filters.getById(flt)
                                            .then(filterdata => {
                                                if (filterdata.enabled) {
                                                    filterdata.filters.forEach(stringFilter => {
                                                        if (alert.mission.reward.asString.search(stringFilter) !== -1) {
                                                            telegramBotApi.sendPhoto({
                                                                chat_id: chatid,
                                                                caption: alert.mission.reward.asString + AL_CAPT,
                                                                photo: alert.mission.reward.thumbnail
                                                            })
                                                        }
                                                    });
                                                }
                                            })
                                    });
                                } else {
                                    // console.log('no filter')
                                    telegramBotApi.sendPhoto({
                                        chat_id: chatid,
                                        caption: alert.mission.reward.asString + AL_CAPT,
                                        photo: alert.mission.reward.thumbnail
                                    })
                                }
                            })
                    });
                }
            });
            currentSentEvents.forEach((alertid, index) => {
                if (!currentEvents.includes(alertid)) {
                    // console.log('deleting')
                    currentSentEvents.splice(index, 1)
                }
            });
        })
}

async function processRequest(message) {
    const userId = message.from.username
    console.log(userId)
    await Users.findByTelegramId(userId)
        .then(data => {
            if (data == null) {
                return telegramBotApi.sendMessage({
                    chat_id: message.chat.id,
                    text: 'You are not registered at wftrack, or didnt specify your telegram id. Please register at wftrack to use this bot'
                })
            }
            if (message.text === '/start') {
                console.log('start')
                data.chatId = message.chat.id
                Users.update(data.login, data)
                    .then(() => {
                        return telegramBotApi.sendMessage({
                            chat_id: message.chat.id,
                            text: 'You are now connected to your wftrack account'
                        })
                    })
            }
            if (message.text === '/getAlerts') {
                // console.log(chatIdsToSendAlerts)
                if (chatIdsToSendAlerts.includes(message.chat.id)) {
                    return telegramBotApi.sendMessage({
                        chat_id: message.chat.id,
                        text: 'You are already subscribed for alert notifications'
                    })
                } else {
                    chatIdsToSendAlerts.push(message.chat.id)
                    return telegramBotApi.sendMessage({
                        chat_id: message.chat.id,
                        text: 'You have just subscribed for alert notifications, to unsubscribe send /stopAlerts'
                    })
                }
            }
            if (message.text === '/stopAlerts') {
                // console.log('im here ?')
                let inToDel
                chatIdsToSendAlerts.forEach((id, index) => {
                    // console.log('checking')
                    if (id === message.chat.id) {
                        inToDel = index
                    }
                });
                // console.log('im here 2')
                if (inToDel !== undefined) {
                    chatIdsToSendAlerts.splice(inToDel, 1);
                    // console.log(chatIdsToSendAlerts)
                }
                return telegramBotApi.sendMessage({
                    chat_id: message.chat.id,
                    text: 'You have just unsubscribed from alert notifications, to subscribe send /getAlerts'
                })
            }
            if (message.text === '/help') {
                console.log('start')
                data.chatId = message.chat.id
                Users.update(data.login, data)
                    .then(() => {
                        return telegramBotApi.sendMessage({
                            chat_id: message.chat.id,
                            text: '/getAlerts to get new alert notifications\n/stopAlerts to stop receiving alert notifications'
                        })
                    })
            } else {
                return telegramBotApi.sendMessage({
                    chat_id: message.chat.id,
                    text: 'Type /help for available commands'
                })
            }
        })
}
