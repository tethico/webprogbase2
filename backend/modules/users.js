const mongoose = require('mongoose');
const Schema = mongoose.Schema;

function User(login, passhash, role, registeredAt, avaUrl, filters, telegramId, chatId) {
    this.login = login;
    this.role = role;
    this.passhash = passhash;
    this.registeredAt = registeredAt;
    this.avaUrl = avaUrl;
    this.filters = filters;
    this.telegramId = telegramId;
    this.chatId = chatId;
}

const UserSchema = new mongoose.Schema({
    login: {type: String, required: true},
    role: {type: Number, required: true},
    registeredAt: {type: Date, required: true},
    passhash: {type: String, required: true},
    avaUrl: {type: String, default: 'http://www.travelcontinuously.com/wp-content/uploads/2018/04/empty-avatar.png'},
    filters: [{type: Schema.Types.ObjectId, ref: 'Filters', default: []}],
    telegramId: {type: String, default: ''},
    chatId: {type: String, default: ''}
});

const UserModel = mongoose.model('User', UserSchema);

function getAll() {
    return UserModel.find();
}

function getById(id) {
    return UserModel.findOne({login: id}).populate('filters');
}

function insert(user) {
    return new UserModel(user).save();
}

function update(login, data) {
    return UserModel.findOneAndUpdate({login: login}, data);
}

function remove(id) {
    return UserModel.findOneAndDelete({login: id});
}

function findByTelegramId(telegId) {
    return UserModel.findOne({telegramId: telegId})
}

function findByChatId(chatId) {
    return UserModel.findOne({chatId: chatId})
}

module.exports = {
    getAll,
    getById,
    insert,
    update,
    remove,
    User,
    findByTelegramId,
    findByChatId
};
