const mongoose = require('mongoose');

function Filter(name, filters, enabled) {
    this.name = name
    this.filters = filters
    this.enabled = enabled
}

const FilterSchema = new mongoose.Schema({
    name: {type: String, required: true},
    filters: [{type: String, required: true}],
    enabled: {type: Boolean, required: true}
});

const FilterModel = mongoose.model('Filters', FilterSchema)

function getById(id) {
    return FilterModel.findById(id)
}

function insert(filter) {
    return new FilterModel(filter).save();
}

function update(id, data) {
    return FilterModel.findOneAndUpdate({_id: id}, data);
}

function remove(id) {
    return FilterModel.findByIdAndDelete(id);
}

module.exports = {
    getById,
    insert,
    update,
    remove,
    Filter
}
