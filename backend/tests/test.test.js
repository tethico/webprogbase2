const {MongoClient} = require('mongodb');

describe('insert', () => {
  let connection;
  let db;

  beforeAll(async () => {
    connection = await MongoClient.connect('mongodb://tethico:rattlesnake12;@ds251223.mlab.com:51223/wftackdb', {
      useNewUrlParser: true
    });
    db = await connection.db('wftackdb');
  });

  beforeEach(async () => {
    await db.collection('test').deleteMany({});
  });

  afterAll(async () => {
    await connection.close();
    await db.close();
  });

  it('should insert a doc into collection', async () => {
    const users = db.collection('test');

    const mockUser = {_id: 'some-user-idw', name: 'John'};
    await users.insertOne(mockUser);

    const insertedUser = await users.findOne({_id: 'some-user-idw'});
    expect(insertedUser).toEqual(mockUser);
  });
});