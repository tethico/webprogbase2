import Vuex from 'vuex'
import Vue from 'vue';

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        isLoggedIn: !!localStorage.getItem('token'),
        currentJwt: null,
        user: localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')) : null,
        ch_a: false,
        ch_i: false,
        ch_f: false,
        ch_s: false
    },
    mutations: {
        change_A(state, isChecked) {
            state.ch_a = isChecked
        },
        change_I(state, isChecked) {
            state.ch_i = isChecked
        },
        change_F(state, isChecked) {
            state.ch_f = isChecked
        },
        change_S(state, isChecked) {
            state.ch_s = isChecked
        },
        LOGIN(state) {
            state.isLoggedIn = true;
        },
        LOGOUT(state) {
            state.isLoggedIn = false;
        }
    },
    actions: {
        login({ commit }, data) {
            return new Promise(resolve => {
                setTimeout(() => {
                    console.log('action login')
                    localStorage.setItem('token', data.token);
                    var base64Url = data.token.split('.')[1];
                    var base64 = base64Url.replace('-', '+').replace('_', '/');
                    this.state.user = JSON.parse(window.atob(base64))
                    localStorage.setItem('user', JSON.stringify(this.state.user));
                    commit('LOGIN');
                    resolve();
                }, 500);
            });
        },
        logout({ commit }) {
            localStorage.removeItem('token')
            localStorage.removeItem('user')
            this.state.user = null;
            commit('LOGOUT');
        }
    }
})
