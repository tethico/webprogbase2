import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Events from '@/components/Events'
import Users from '@/components/Users'
import User from '@/components/User'
import Login from '@/components/Login'
import Register from '@/components/Register'
import Api from '@/components/Api'

Vue.use(Router)

let router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/docs',
      name: 'api',
      component: Api,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/',
      name: 'home',
      component: Home,
      meta: {
        guest: true
      }
    },
    {
      path: '/events',
      name: 'events',
      component: Events,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/users',
      name: 'users',
      component: Users,
      meta: {
        requiresAuth: true,
        is_admin: true
      }
    },
    {
      path: '/users/:id',
      name: 'userpage',
      component: User,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/login',
      name: 'login',
      component: Login,
      meta: {
        guest: true
      }
    },
    {
      path: '/register',
      name: 'register',
      component: Register,
      meta: {
        guest: true
      }
    }
  ]
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    // console.log(localStorage.getItem('token'))
      if (localStorage.getItem('token') == null) {
          // console.log('req auth no token')
          next({
              path: '/login',
              params: { nextUrl: to.fullPath }
          })
      } else {
          let user = JSON.parse(localStorage.getItem('user'))
          if (to.matched.some(record => record.meta.is_admin)) {
              if (user.role === 1) {
                  next()
              } else {
                // console.log('req auth no admin')
                  next({name: 'home'})
              }
          } else {
              next()
          }
      }
  } else if (to.matched.some(record => record.meta.guest)) {
      if (localStorage.getItem('token') == null) {
        // console.log('no auth no token')
        next()
      } else {
        // console.log('have token')
          next()
      }
  } else {
      next()
  }
})

export default router
